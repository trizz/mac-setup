# Define the exports.
export PATH=$HOME/bin:/usr/local/sbin:/usr/local/bin:$HOME/.composer/vendor/bin:$PATH
export ZSH=$HOME/.oh-my-zsh

# Reset the theme. Pure is used as a theme manager.
ZSH_THEME=""

# Source the different directories
source $HOME/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source $HOME/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.zsh/aliases.zsh
source $ZSH/oh-my-zsh.sh

plugins=(composer laravel5 sudo zsh-autosuggestions zsh-syntax-highlighting)

autoload -U promptinit; promptinit
prompt pure
